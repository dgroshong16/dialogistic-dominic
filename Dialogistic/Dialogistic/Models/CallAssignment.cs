namespace Dialogistic.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CallAssignment
    {
        [Key]
        [Column(Order = 0)]
        public string CallerID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ConstituentID { get; set; }

        public int? CallDetailsID { get; set; }

        public virtual CallDetail CallDetail { get; set; }

        public virtual Constituent Constituent { get; set; }
    }
}
