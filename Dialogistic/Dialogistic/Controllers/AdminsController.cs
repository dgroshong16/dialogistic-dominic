﻿using Dialogistic.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;

using System.Text.RegularExpressions;

namespace Dialogistic.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminsController : Controller
    {
        private ApplicationUserManager _userManager;

        private DialogisticContext db = new DialogisticContext();

        public AdminsController()
        {            
        }

        public AdminsController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        /// <summary>
        /// Loads the dashboard for admins, grabbing important information to display.
        /// </summary>
        /// <returns>The dashboard view if the user is authenticated, and the login page if not.</returns>
        [HttpGet]        
        public ActionResult Dashboard()
        {
            if (User.Identity.IsAuthenticated)
            {
                // Get the current user
                var currentUser = User.Identity.GetUserId();
                ViewBag.UserProfile = db.UserProfiles.Where(x => x.UserID.Equals(currentUser));
                
                // Grab all of the calls still needing to be made
                ViewBag.TotalRemainingCalls = db.CallAssignments.Count();

                // Grab the total donations raised by all callers
                var totalDonations = db.UserProfiles.Sum(x => x.DonationsRaised).ToString();
                ViewBag.TotalDonations = String.Format("{0:c}", totalDonations);
                return View();
            }

            // If we got this far, the user isn't authenticated - send them to the login screen
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Loads the overlay with the current user's information displayed.
        /// </summary>
        /// <returns>The site overlay partial view.</returns>
        [ChildActionOnly]
        public PartialViewResult _SiteOverlay()
        {
            // Grab the current user
            var currentUserID = User.Identity.GetUserId();
            if (currentUserID != null)
            {
                // Grab their profile so we can display their information
                UserProfile user = db.UserProfiles.Where(x => x.UserID.Equals(currentUserID)).FirstOrDefault();

                // Return the partial view with the user profile
                return PartialView(user);
            }

            // If we got this far, we couldn't find a user associated with their ID - return the view without it
            return PartialView();
        }

        // POST: /Admins/LogOff
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Generates a list of call assignments from constituents whos last contact date was more than 90 days
        /// ago, or is null. A random user designated as a caller is assigned to each constituent.
        /// </summary>
        /// <returns>A list of CallAssignments -- Will be empty if there are no Constituents that need to be contacted</returns>
        public ActionResult GenerateCallList()
        {
            ///////////////////////////////////////////////////////////////////////////////
            //                                                                           //
            // TO-DO: REFACTOR TO INCLUDE MORE DATA WHEN GENERATING THE CALL ASSIGNMENTS //            
            //                                                                           //
            ///////////////////////////////////////////////////////////////////////////////

            // Set the threshold for determining if a Constituent needs to be added to the call assignments - (90 days ago from today)
            DateTime contactThreshold = DateTime.Now.AddDays(-90);            
            
            // Build the list of Constituents that need to be called - (last contacted date > 90 days ago, or null)
            List<Constituent> constituents = db.Constituents.Where(x => x.LastContacted < contactThreshold).ToList();
            constituents.AddRange(db.Constituents.Where(x => x.LastContacted == null).ToList());

            // Only build the list if there are Constituents that need to be contacted
            if (constituents.Count != 0)
            {
                // Create an instance of Random to use in the loop below
                Random rnd = new Random();

                // Create an empty list of Call Assignments
                List<CallAssignment> callList = new List<CallAssignment>();

                // Generate a list of callers from users designated as such
                List<UserProfile> callers = db.UserProfiles.Where(x => x.IsCaller).ToList();

                // Loop through the list of Constituents, assigning a random caller each time we add to the list                
                foreach (var item in constituents)
                {
                    // Create a new Call Assignment using the current item's ConstituentID and a random CallerID from the list of callers
                    callList.Add(new CallAssignment() { CallerID = callers[rnd.Next(callers.Count())].UserID, ConstituentID = item.ConstituentID });
                }

                // Add the assignments to the database, and save the changes
                db.CallAssignments.AddRange(callList);
                db.SaveChanges();
                
                return RedirectToAction("ManageCallList", "Admins", callList);
            }
            
            // Return the list -- this will be an empty list if there were no Constituents that needed to be contacted
            return RedirectToAction("ManageCallList", "Admins");
        }

        /// <summary>
        /// Loads the view where admins can manage which Callers are calling which Constituents.
        /// </summary>
        /// <returns>The view loaded with the current list of Call Assignments</returns>
        [HttpGet]
        public ActionResult ManageCallList()
        {
            // Build a list of the current Call Assignments
            List<CallAssignment> callList = callList = db.CallAssignments.ToList();

            // Grab all of the Users designated as Callers
            ViewBag.Callers = db.UserProfiles.Where(x => x.IsCaller);

            // Build a list of the current Call Assignments, ordered by ConstituentID
            callList = db.CallAssignments.OrderBy(x => x.ConstituentID).ToList();

            // Return the view with the current list of Call Assignments
            return View(callList);
        }

        /// <summary>
        /// Updates the provided Call Assignment with the new Caller.
        /// </summary>
        /// <param name="model">The Call Assignment to update.</param>
        /// <returns>The Manage Call List view on success, and the Dashboard on failure.</returns>
        [HttpPost]
        public ActionResult ManageCallList(CallAssignment model)
        {
            // Make sure the model is in a valid state before we do anything with it
            if (ModelState.IsValid)
            {
                // Grab the original entry from the table
                CallAssignment original = db.CallAssignments.Where(x => x.ConstituentID.Equals(model.ConstituentID)).FirstOrDefault();
                if (original != null)
                {
                    // If we found it, remove it
                    db.CallAssignments.Remove(original);

                    // Set the model's Constituent to the appropriate one from the Constituent table, based on their ID
                    model.Constituent = db.Constituents.Find(model.ConstituentID);

                    // Add the new entry to the Call Assignments table and save the changes to the database
                    db.CallAssignments.Add(model);
                    db.SaveChanges();
                }

                // Build a list of all the current Call Assignments, ordered by ConstituentID
                //List<CallAssignment> callList = new List<CallAssignment>();
                //callList = db.CallAssignments.OrderBy(x => x.ConstituentID).ToList();

                // Return this view
                return RedirectToAction("ManageCallList");
            }

            // If we got this far, the model state wasn't valid - redirect to the Dashboard
            return RedirectToAction("Dashboard", "Admins");
        }
    }
}