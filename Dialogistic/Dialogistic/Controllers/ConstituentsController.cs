﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Dialogistic.Models;
using PagedList;

namespace Dialogistic.Controllers
{
    [Authorize(Roles = "Administrator, Standard")]
    public class ConstituentsController : Controller
    {
        //private ConstituentContext db = new ConstituentContext();
        private DialogisticContext db = new DialogisticContext();

        // GET: Constituents
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            // Set ViewBag parameters for searching and sorting
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PreferredCitySortParm = sortOrder == "PreferredCity" ? "preferredcity_desc" : "PreferredCity";
            ViewBag.PreferredStateSortParm = sortOrder == "PreferredState" ? "preferredstate_desc" : "PreferredState";
            ViewBag.ContactDateSortParm = sortOrder == "ContactDate" ? "contactdate_desc" : "ContactDate";
            ViewBag.GiftDateSortParm = sortOrder == "GiftDate" ? "giftdate_desc" : "GiftDate";

            // Set page and search string information
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            // Set the ViewBag parameter for the search string
            ViewBag.CurrentFilter = searchString;

            // Get all entries in the Constituents table
            var constituents = from s in db.Constituents
                               select s;

            // If the search string isn't null or empty, attempt to search for that substring
            if (!String.IsNullOrEmpty(searchString))
            {
                // Search all the entries by the PrimaryAddressee field
                constituents = constituents.Where(s => s.PrimaryAddressee.Contains(searchString));

                // Set the SearchMessage based on the above results
                var searchTotal = constituents.ToList();
                if (searchTotal.Count() >= 1)
                {
                    ViewBag.CurrentSearch = searchString;
                    ViewBag.SearchMessage = "Constituents matching your search: " + searchString;
                }
                else
                {
                    ViewBag.SearchMessage = "There we no constituents matching your search: " + searchString;
                }
            }

            // Perform the correct sort based on sortOrder
            switch (sortOrder)
            {
                //case "name_desc":
                //    constituents = constituents.OrderByDescending(s => s.PrimaryAddressee);
                //    break;
                case "PreferredCity":
                    constituents = constituents.OrderBy(s => s.PreferredCity);
                    break;
                case "preferredcity_desc":
                    constituents = constituents.OrderByDescending(s => s.PreferredCity);
                    break;
                case "PreferredState":
                    constituents = constituents.OrderBy(s => s.PreferredState);
                    break;
                case "preferredstate_desc":
                    constituents = constituents.OrderByDescending(s => s.PreferredState);
                    break;
                case "ContactDate":
                    constituents = constituents.OrderBy(s => s.LastContacted);
                    break;
                case "contactdate_desc":
                    constituents = constituents.OrderByDescending(s => s.LastContacted);
                    break;
                case "GiftDate":
                    constituents = constituents.OrderBy(s => s.LastGiftDate);
                    break;
                case "giftdate_desc":
                    constituents = constituents.OrderByDescending(s => s.LastGiftDate);
                    break;
                default:  // ConstituentID ascending 
                    constituents = constituents.OrderBy(s => s.ConstituentID);
                    break;
            }

            // Set the pagination parameters and return the view
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(constituents.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateConstituent(Constituent constituent)
        {
            if (ModelState.IsValid)
            {
                db.Constituents.Add(constituent);
                db.SaveChanges();

                return RedirectToAction("Index", "Constituents");
            }

            return View(constituent);
        }

        [HttpPost]
        public bool Delete(int id)
        {
            // If this constituent is in the call assignments, remove them from there before deleting
            CallAssignment callAssignment = db.CallAssignments.Where(x => x.ConstituentID == id).FirstOrDefault();
            if (callAssignment != null)
            {
                db.CallAssignments.Remove(callAssignment);

                // Remove the constituent from the database
                Constituent constituent = db.Constituents.Where(x => x.ConstituentID == id).FirstOrDefault();
                if (constituent != null)
                {
                    db.Constituents.Remove(constituent);
                    db.SaveChanges();
                }
                return true;
            }
            else
            {
                // Remove the constituent from the database
                Constituent constituent = db.Constituents.Where(x => x.ConstituentID == id).FirstOrDefault();
                if (constituent != null)
                {
                    db.Constituents.Remove(constituent);
                    db.SaveChanges();
                }
                return true;
            }
        }

        [HttpGet]
        public ActionResult Update(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Constituent constituent = db.Constituents.Find(id);
            if (constituent == null)
            {
                return HttpNotFound();
            }            

            return View(constituent);
        }

        [HttpPost]
        public ActionResult UpdateConstituent(Constituent model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", "Constituents");
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
