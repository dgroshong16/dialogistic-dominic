﻿using Dialogistic.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dialogistic.Controllers
{
    [Authorize(Roles = "Standard")]
    public class StandardUsersController : Controller
    {
        private ApplicationUserManager _userManager;

        private DialogisticContext db = new DialogisticContext();

        public StandardUsersController()
        {
        }

        public StandardUsersController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        // GET: StandardUsers
        [HttpGet]
        public ActionResult Dashboard()
        {
            if (User.Identity.IsAuthenticated)
            {
                var currentUser = User.Identity.GetUserId();
                UserProfile standardUser = db.UserProfiles.Where(x => x.UserID == currentUser).First();
                standardUser.CallsRemaining = db.CallAssignments.Where(x => x.CallerID == standardUser.UserID).Count();
           
                return View(standardUser);
            }


            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult ViewCallList()
        {
            var currentUser = User.Identity.GetUserId();
            UserProfile standardUser = db.UserProfiles.Where(x => x.UserID == currentUser).First();
            List<CallAssignment> callAssignments = db.CallAssignments.Where(x => x.CallerID == standardUser.UserID).ToList();

            return View(callAssignments);
        }

        [ChildActionOnly]
        public PartialViewResult _SiteOverlay()
        {
            var currentUserID = User.Identity.GetUserId();
            if (currentUserID != null)
            {
                UserProfile user = db.UserProfiles.Where(x => x.UserID.Equals(currentUserID)).FirstOrDefault();

                return PartialView(user);
            }

            return PartialView();
        }

        // POST: /StandardUsers/LogOff
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }
    }
}