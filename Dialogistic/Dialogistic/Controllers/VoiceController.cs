﻿using Dialogistic.Models;
using System.Configuration;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Twilio.TwiML;
using Twilio.TwiML.Voice;

namespace Dialogistic.Controllers
{
    public class VoiceController : Controller
    {
        // Instantiate an instance of our DB context file
        DialogisticContext db = new DialogisticContext();

        /// <summary>
        /// Builds a view for a given Constituent where a Caller can call them from.
        /// </summary>
        /// <param name="id">The ID of the Constituent to build the view for.</param>
        /// <returns>The view if the there's a Constituent associated with the given ID, HttpNotFound otherwise.</returns>
        [HttpGet]
        public ActionResult Index(int? id)
        {
            // If id is null, return an error
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // It wasn't null, so try and find a Constituent associated with it
            Constituent constituent = db.Constituents.Find(id);

            // If we can't find a Constituent with this id, return an error
            if (constituent == null)
            {
                return HttpNotFound();
            }

            // Return the view with the Consituent's information
            return View(constituent);
        }

        /// <summary>
        /// Gets the necessary information from the form in order to have Twilio call
        /// a Constituent.
        /// </summary>
        /// <param name="to"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(string to)
        {
            // If there's a caller ID setup, grab it here
            var callerId = ConfigurationManager.AppSettings["TwilioCallerId"];

            // Instantiate a new VoiceResponse
            var response = new VoiceResponse();

            // If 'to' isn't empty or null, this is an outgoing call -- this should always have something
            if (!string.IsNullOrEmpty(to))
            {
                // Build the Dial object for placing the call
                var dial = new Dial(callerId: callerId);
                
                /* Wrap the phone number or Constituent name in the appropriate TwiML verb
                   by checking if the number given has only digits and format symbols */
                if (Regex.IsMatch(to, "^[\\d\\+\\-\\(\\) ]+$"))
                {
                    dial.Number(to);
                }
                else
                {
                    dial.Client(to);
                }

                // Append the caller ID
                response.Append(dial);
            }
            // 'to' was empty or null -- this is an incoming call
            else
            {
                // Default voice playback when an incoming call is placed -- not going to be used
                response.Say("Thanks for calling!");
            }

            // Send the information to Twilio
            return Content(response.ToString(), "text/xml");
        }
    }
}