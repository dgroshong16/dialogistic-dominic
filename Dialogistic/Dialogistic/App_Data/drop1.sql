--ALTER TABLE [dbo].[CallDetails]
--	DROP CONSTRAINT [FK_dbo.CallDetails_dbo.AspNetUsers_UserId], [FK_dbo.CallDetails_dbo.Constituents_ConsituentID];

--ALTER TABLE [dbo].[UserProfiles]
--	DROP CONSTRAINT [FK_dbo.UserProfiles_dbo.AspNetUsers_UserId];

--ALTER TABLE [dbo].CallAssignments
--	DROP CONSTRAINT [FK_dbo.Calls.Constituents_ConstituentID], [FK_dbo.Calls.UserProfiles_CallerID];


--DROP TABLE [dbo].[CallAssignments];
--DROP TABLE [dbo].[CallDetails];
--DROP TABLE [dbo].[UserProfiles];
--DROP TABLE [dbo].[Constituents];
--DROP TABLE [dbo].[AspNetUserRoles];
--DROP TABLE [dbo].[AspNetUserLogins];
--DROP TABLE [dbo].[AspNetUserClaims];
--DROP TABLE [dbo].[AspNetRoles];
--DROP TABLE [dbo].[AspNetUsers];


