INSERT INTO [dbo].[Callers](UserID) VALUES
('38282f97-aa48-47b2-b42b-82c81bc2e075'),
('38282f97-aa48-47b2-b42b-82c81bc2e075');


INSERT INTO [dbo].[Constituents](Salutation, GenderPronoun, FirstName, LastName, Address1, Address2, City, State, Zip, PhoneNumber, MaritalStatus, BirthDate, Deceased, LastContacted, LastDonated) VALUES
('Mr', 'He/His/Him', 'Clifford', 'Claven', '123 Beacon St.', '', 'Boston', 'MA', '87764', '5552223333', 'Single', 1950-01-02, 0, 1999-09-09, 1999-09-09),
('Mrs', 'She/Her/Hers', 'Dannie', 'Devito', '188 Runner St.', '', 'Alpine', 'WA', '12345', '6657789900', 'Married', 1959-12-21, 0, 1999-09-09, 1999-09-09),
('Mr', 'He/His/Him', 'John', 'Stamos', '899 Sycamore', '', 'Seattle', 'CA', '82521', '1223345511', 'Single', 1958-01-02, 0, 1999-09-09, 1999-09-09),
('Ms', 'She/Her/Hers', 'Carrie', 'Underwood', '554 Cheatin Dr.', '', 'Houston', 'TX', '11442', '9909909900', 'Divorced', 1492-07-02, 0, 1999-09-09, 1999-09-00),
('Mr', 'He/His/Him', 'Carson', 'Palmer', '0 Bengals Way', '', 'Cinncinati', 'OH', '54433', '5416527587', 'Single', 1978-01-01, 0, 1999-09-09, 1999-09-09);


INSERT INTO [dbo].[Calls](DateOfCall, CallAnswered, LineAvailable) VALUES
(2019-02-01, 1, 1),
(2019-01-01, 1, 1),
(2018-01-01, 1, 1),
(2017-08-08, 1, 1),
(2016-01-11, 1, 1),
(2016-12-25, 0, 1),
(2016-09-23, 1, 1);