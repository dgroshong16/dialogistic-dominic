namespace Dialogistic.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DialogisticContext : DbContext
    {
        public DialogisticContext()
            : base("name=DialogisticContext")
        {
        }

        public virtual DbSet<CallAssignment> CallAssignments { get; set; }
        public virtual DbSet<CallDetail> CallDetails { get; set; }
        public virtual DbSet<Constituent> Constituents { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Conversation> Conversations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CallDetail>()
                .HasMany(e => e.CallAssignments)
                .WithOptional(e => e.CallDetail)
                .HasForeignKey(e => e.CallDetailsID);

            modelBuilder.Entity<Constituent>()
                .HasMany(e => e.CallAssignments)
                .WithRequired(e => e.Constituent)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Constituent>()
                .HasMany(e => e.CallDetails)
                .WithRequired(e => e.Constituent)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .Property(e => e.DonationsRaised)
                .HasPrecision(19, 4);
        }
    }
}
