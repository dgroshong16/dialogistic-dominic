﻿-- Takes the database tables down

-- This removes the constraints on the tables in the appropriate order.
ALTER TABLE Comments DROP CONSTRAINT [FK_dbo.Comments];
ALTER TABLE Comments DROP CONSTRAINT [FK2_dbo.Comments];
ALTER TABLE Discussions DROP CONSTRAINT [FK_dbo.Discussions];

-- This drops the four tables.
DROP TABLE [dbo].[SupportTickets]
DROP TABLE [dbo].[Comments];
DROP TABLE [dbo].[Discussions];
DROP TABLE [dbo].[Posts];
DROP TABLE [dbo].[Users];
DROP TABLE [dbo].[SupportTickets];
