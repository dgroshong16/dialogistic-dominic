namespace PracticeProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Post
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Post()
        {
            Discussions = new HashSet<Discussion>();
        }

        [Key]
        public int PostsID { get; set; }

        [Required]
        [StringLength(48)]
        [DisplayName("Post Title:")]
        public string Title { get; set; }

        [DisplayName("Name:")]
        public int UsersID { get; set; }

        [Required]
        [StringLength(1000)]
        [DisplayName("Post Your Content:")]
        public string PostContent { get; set; }

        private DateTime date = DateTime.Now;
        [DisplayName("Time Placed")]
        public DateTime DateTimeOfPost {
            get { return date; }
            set { date = value; }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Discussion> Discussions { get; set; }

        [DisplayName("Username:")]
        public virtual User User { get; set; }
    }
}
